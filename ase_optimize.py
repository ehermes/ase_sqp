import numpy as np

from ase.optimize import BFGS


class BetterBFGS(BFGS):
    def __init__(self, *args, maxstep=0.2, **kwargs):
        # change default maxstep to 0.2 (from 0.04)
        BFGS.__init__(self, *args, maxstep=0.2, **kwargs)

    def update(self, *args, **kwargs):
        if self.H is None:
            # also change initial curvature to 10 (from 70)
            self.H = 10 * np.eye(3 * len(self.atoms))
            return
        return BFGS.update(self, *args, **kwargs)
