from ase.io import read
from ase_optimize import BetterBFGS
from sqp_optimize import SQPBFGS
from sqp_constraints import SQPFixBondLengths
from ase.calculators.dftb import Dftb
from ase.calculators.socketio import SocketIOCalculator

sockname = 'sqp_test'

dftb_kwargs = {'command': '/home/ehermes/build/dftbplus-master/_build/prog/dftb+/dftb+ > PREFIX.out',
               'slako_dir': '/home/ehermes/local/3ob-3-1/',
               'Hamiltonian_Charge': 0.,
               'Hamiltonian_SCC': 'YES',
               'Hamiltonian_SCCTolerance': 1e-8,
               'Hamiltonian_MaxSCCIterations': 5000,
               'Hamiltonian_ShellResolvedSCC': 'YES',
               'Hamiltonian_MaxAngularMomentum_': '',
               'Hamiltonian_MaxAngularMomentum_H': '"s"',
               'Hamiltonian_MaxAngularMomentum_C': '"p"',
               'Hamiltonian_MaxAngularMomentum_O': '"p"',
               'Hamiltonian_ThirdOrderFull': 'YES',
               'Hamiltonian_HCorrection_': 'Damping',
               'Hamiltonian_HCorrection_Exponent': 4.0,
               'Hamiltonian_HubbardDerivs_': '',
               'Hamiltonian_HubbardDerivs_H': -0.1857,
               'Hamiltonian_HubbardDerivs_C': -0.1492,
               'Hamiltonian_HubbardDerivs_O': -0.1575,
               'Hamiltonian_Dispersion': 'DftD3{}',
               'Hamiltonian_SpinPolarisation_': 'Colinear',
               'Hamiltonian_SpinPolarisation_UnpairedElectrons': 1,
               'Hamiltonian_SpinConstants_': '',
               'Hamiltonian_SpinConstants_ShellResolvedSpin': 'YES',
               'Hamiltonian_SpinConstants_H': '{ -0.07174 }',
               'Hamiltonian_SpinConstants_C': '{ -0.03062 -0.02505 -0.02505 -0.02265 }',
               'Hamiltonian_SpinConstants_O': '{ -0.03524 -0.02956 -0.02956 -0.02785 }',
               'Driver_': 'Socket',
               'Driver_File': sockname,
               'Driver_Protocol': 'i-PI {}',
              }

atoms = read('test_mol.xyz')
pairs = [[0, 1], [1, 2], [2, 3], [2, 7], [3, 4], [3, 8], [4, 5], [4, 10], [5, 6], [5, 11], [5, 12], [6, 13], [6, 14], [6, 15], [0, 9], [4, 9]]
atoms.set_constraint(SQPFixBondLengths(pairs))
pos0 = atoms.positions.copy()

dftb = Dftb(**dftb_kwargs)

with SocketIOCalculator(dftb, unixsocket=sockname) as calc:
    atoms.calc = calc

    # First try the SQP version of BFGS
    dyn_sqp = SQPBFGS(atoms, trajectory='test_mol_sqp.traj')
    dyn_sqp.run(fmax=0.01)

    # Then try ASE's BFGS (or rather, an improved version thereof)
    atoms.positions = pos0.copy()
    dyn_ase = BetterBFGS(atoms, trajectory='test_mol_ase.traj')
    dyn_ase.run(fmax=0.01)  # Constraints raise a RuntimeError
