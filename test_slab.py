import numpy as np
from itertools import combinations

from ase import Atoms
from ase.build import fcc111, add_adsorbate
from ase.calculators.emt import EMT
from ase_optimize import BetterBFGS
from sqp_optimize import SQPBFGS
from sqp_constraints import SQPFixBondLengths

rng = np.random.RandomState(2)

slab = fcc111('Al', (5, 5, 5), vacuum=10)
nslab = len(slab)

ads_pos = np.array([[0.0, 0.0, 0.0],
                    [0.0, 3.0, 0.0],
                    [1.5 * np.sqrt(3.), 1.5, 0.0],
                    [np.sqrt(3) / 2., 1.5, 3.0 * np.sqrt(2./3.)]])

ads = Atoms(['Au'] * 4, ads_pos)
ads.rotate(rng.random() * 360, 'x')
ads.rotate(rng.random() * 360, 'y')
ads.rotate(rng.random() * 360, 'z')

add_adsorbate(slab, ads, 3.0, position=slab.cell[:2, :2].sum(0) / 2.)
pos0 = slab.positions.copy()

# get a rattled geometry before adding the constraint.
# I *hate* that constraints affect the behavior of stuff like rattle.
slab.rattle(0.1, rng=rng)
pos1 = slab.positions.copy()
slab.positions = pos0.copy()

pairs = []
for i, j in combinations(range(4), 2):
    pairs.append([nslab + i, nslab + j])

slab.calc = EMT()
slab.set_constraint(SQPFixBondLengths(pairs))

# First run SQP optimizer
dyn_sqp = SQPBFGS(slab, trajectory='test_slab_sqp.traj')
dyn_sqp.run(fmax=0.01)

# Then run ASE's native optimizer
slab.positions = pos0.copy()
dyn_ase = BetterBFGS(slab, trajectory='test_slab_ase.traj')
dyn_ase.run(fmax=0.01)

# rattle the atoms and try again
slab.positions = pos1.copy()
dyn_sqp2 = SQPBFGS(slab, trajectory='test_slab_rattle_sqp.traj')
dyn_sqp2.run(fmax=0.01)

slab.positions = pos1.copy()
dyn_ase2 = BetterBFGS(slab, trajectory='test_slab_rattle_ase.traj')
dyn_ase2.run(fmax=0.01)
