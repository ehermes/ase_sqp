import numpy as np
from time import localtime, strftime

from ase_optimize import BetterBFGS


class SQPBFGS(BetterBFGS):
    def step(self, f=None):
        atoms = self.atoms

        if f is None:
            f = self.atoms.get_forces(apply_constraint=False)

        r = atoms.get_positions()
        f = f.ravel()
        self.update(r.ravel(), f, self.r0, self.f0)
        # Here's where the difference begins
        H, g, sy, Z = self.project_constraints(-f)
        # H is the Hessian of the Lagrangian projected into Z
        # g is the gradient extrapolated to r + sy projected into Z
        # sy is the linear constraint correction step
        # Z is the unitary projection matrix spanning the tangent space of
        omega, V = np.linalg.eigh(H)
        s = (sy - Z @ V @ ((g @ V) / np.abs(omega))).reshape((-1, 3))
        steplengths = np.sqrt((s**2).sum(1))

        # This is kind of bad, because it will also scale back the constraint
        # correction step, which I think it shouldn't... but fixing that is
        # more complicated than it's worth for now
        s = self.determine_step(s, steplengths)
        atoms.set_positions(r + s)
        self.r0 = r.ravel().copy()
        self.f0 = f.copy()
        self.dump((self.H, self.r0, self.f0, self.maxstep))

    def project_constraints(self, g):
        # Hessian of the PES
        if not self.atoms.constraints:
            n = len(g)
            return self.H, g, np.zeros(n), np.eye(n)

        # In reality, atoms.constraints ought to be some sort of object
        # that can synthesize a residual vector, jacobian, and jacobian
        # derivative from a collection of disparate constraints.
        # For the purposes of this demo, we assume atoms.constraints
        # contains precisely one SQP constraint object
        cons = self.atoms.constraints[0]
        res, J, dJ = cons.get_residual(self.atoms, True, True)

        # Lagrange multipliers
        lmult = np.linalg.lstsq(J.T, g, rcond=None)[0]

        # Hessian of the Lagrangian
        Hlag = self.H - dJ.left_prod(lmult)

        # partitioning into normal and tangent space
        _, S, VT = np.linalg.svd(J, full_matrices=True)
        ny = np.sum(S > 1e-8)  # this tolerance should be a parameter
        Y = VT[:ny].T
        Z = VT[ny:].T

        # Projected Hessian
        Hproj = Z.T @ Hlag @ Z

        # Constraint correction vector
        sy = Y @ np.linalg.lstsq(J @ Y, -res, rcond=None)[0]

        # Effective projected gradient
        geff = Z.T @ (g + self.H @ sy)

        # Projected Hessian (of the Lagrangian)
        Hproj = Z.T @ Hlag @ Z

        return Hproj, geff, sy, Z

    def project_forces(self, forces):
        if not self.atoms.constraints:
            return forces
        cons = self.atoms.constraints[0]
        res, J = cons.get_residual(self.atoms, True, False)
        P = np.linalg.pinv(J) @ J
        return forces - (forces.ravel() @ P).reshape((-1, 3))

    ################################################################
    # Every method defined below is essentially identical to the   #
    # method in the base Optimizer/Dynamics class, except that all #
    # calls to self.atoms.get_forces() has been replaced with      #
    # self.atoms.get_forces(apply_constraint=False).               #
    ################################################################

    def log(self, forces=None):
        if forces is None:
            forces = self.atoms.get_forces(apply_constraint=False)
        f_proj = self.project_forces(forces)
        if not self.atoms.constraints:
            BetterBFGS.log(self, forces=f_proj)
        # also add a column for constraint err
        res = self.atoms.constraints[0].get_residual(self.atoms, False, False)
        cmax = np.max(np.abs(res))
        fmax = np.sqrt((f_proj**2).sum(1).max())
        e = self.atoms.get_potential_energy()
        T = strftime("%H:%M:%S", localtime())
        if self.logfile is None:
            return
        name = self.__class__.__name__
        buf = " " * len(name)
        if self.nsteps == 0:
            self.logfile.write(buf + "{:>4s} {:>8s} {:>15s} {:>12s} {:>12s}\n"
                               .format("Step", "Time", "Energy", "fmax",
                                       "cmax"))
        self.logfile.write("{} {:>3d} {:>8s} {:>15.6f} {:>12.4f} {:>12.4f}\n"
                           .format(name, self.nsteps, T, e, fmax, cmax))
        self.logfile.flush()

    def converged(self, forces=None):
        if not self.atoms.constraints:
            return BetterBFGS.log(self, forces)
        atoms = self.atoms
        cons = atoms.constraints[0]
        if forces is None:
            forces = atoms.get_forces(apply_constraint=False)
        f_proj = self.project_forces(forces)
        return cons.converged(atoms) and BetterBFGS.converged(self, f_proj)

    def irun(self):
        self.atoms.get_forces(apply_constraint=False)

        yield False

        if self.nsteps == 0:
            self.log()
            self.call_observers()

        while not self.converged() and self.nsteps < self.max_steps:
            self.step()
            self.nsteps += 1
            yield False
            self.log()
            self.call_observers()
        yield self.converged()

    def run(self, fmax=0.05, steps=None):
        self.fmax = fmax
        if steps:
            self.max_steps = steps
        for converged in self.irun():
            pass
        return converged
