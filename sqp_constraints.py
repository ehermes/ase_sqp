import numpy as np
from ase.geometry import find_mic
from ase.constraints import FixBondLengths


# Ideally we would use an off-the-shelf solution from SciPy, but scipy.sparse
# is only for 2d data (i.e. matrices), and the derivative of the Jacobian is
# a 3-index tensor.
class JacDeriv:
    def __init__(self, indices, djacs, natoms):
        self.indices = indices
        self.djacs = djacs
        self.natoms = natoms

    # This tensor is a 3-index quantity, but the last two indices are
    # equivalent/interchangeable because they correspond to indices of
    # the Hessian, which is a symmetric quantity. We refer to reduction
    # over the first index as a "left product" and reduction over either
    # of the other two indices as a "right product". Reduction over both
    # of the two right indices can be implemented efficiently as a single
    # operation, hence "double right product".

    def left_prod(self, v):
        """Performs the tensor contraction dJ_ijk * v_i"""
        assert v.size == len(self.indices)
        out = np.zeros((self.natoms, 3, self.natoms, 3))
        # there may be a more efficient way of doing this than a double loop,
        # but I'm not sure what it is.
        for vij, indices, dJij in zip(v, self.indices, self.djacs):
            for n, i in enumerate(indices):
                out[indices, :, i, :] += vij * dJij[:, :, n, :]
        return out.reshape((3 * self.natoms, 3 * self.natoms))

    # the current test doesn't use either of the two products defined below,
    # but here they are anyway for completeness
    def right_prod(self, v):
        """Performs the tensor contraction dJ_ijk * v_k"""
        assert v.size == 3 * self.natoms
        v = v.reshape((self.natoms, 3))

        out = np.zeros((len(self.indices), self.natoms, 3))
        for n, (indices, dJij) in enumerate(zip(self.indices, self.djacs)):
            out[n, indices, :] = np.einsum('ijkl,kl->ij', dJij, v[indices, :])
        return out.reshape((-1, 3 * self.natoms))

    def double_right_prod(self, v, w):
        """Performs the tensor contraction dJ_ijk * v_j * w_k"""
        assert v.size == w.size == 3 * self.natoms
        v = v.reshape((self.natoms, 3))
        w = w.reshape((self.natoms, 3))
        out = np.zeros(len(self.indices))
        for n, (idx, dJij) in enumerate(zip(self.indices, self.djacs)):
            out[n] = np.einsum('ijkl,ij,kl', dJij, v[idx, :], w[idx, :])
        return out

    # JacDerivs can be concatenated together
    def __add__(self, other):
        assert isinstance(other, JacDeriv)
        assert self.natoms == other.natoms
        return JacDeriv(self.indices + other.indices,  # list concat.
                        self.djacs + other.djacs,      # ibid
                        self.natoms)


class SQPFixBondLengths(FixBondLengths):
    def __init__(self, *args, tolerance=1e-5, **kwargs):
        FixBondLengths.__init__(self, *args, tolerance=tolerance, **kwargs)

    def converged(self, atoms):
        return np.all(np.abs(self.get_residual(atoms)) < self.tolerance)

    def get_residual(self, atoms, jac=False, djac=False):
        """Calculates the residual (error) vector for constraints."""
        if self.bondlengths is None:
            self.bondlengths = self.initialize_bond_lengths(atoms)

        dists = np.zeros(len(self.pairs))
        vecs = np.zeros((len(self.pairs), 3))

        for n, (i, j) in enumerate(self.pairs):
            xij = atoms.positions[j] - atoms.positions[i]
            vecs[n], dists[n] = find_mic(xij, atoms.cell, atoms.pbc)

        res = dists - np.asarray(self.bondlengths)
        if not (jac or djac):
            return res

        J = np.zeros((len(self.pairs), len(atoms), 3))
        dJ = []

        for n, ((i, j), dij, xij) in enumerate(zip(self.pairs, dists, vecs)):
            deriv = xij / dij
            J[n, i] = -deriv
            J[n, j] = deriv
            if not djac:
                continue
            dJij = np.zeros((2, 3, 2, 3))  # e.g. would be 3x3x3x3 for angle
            dJij[0, :, 0, :] = np.eye(3) / dij - np.outer(xij, xij) / dij**3
            dJij[1, :, 1, :] = dJij[0, :, 0, :]
            dJij[1, :, 0, :] = dJij[0, :, 1, :] = -dJij[0, :, 0, :]
            dJ.append(dJij)
        J = J.reshape((-1, 3 * len(atoms)))

        if not djac:
            return res, J
        return res, J, JacDeriv(self.pairs, dJ, len(atoms))
